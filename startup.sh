#! /bin/bash

cd $ADAM_STEVENZ_PROJ
export CHROMEDRIVER_PATH=$ADAM_STEVENZ_PROJ'/windows_chromedriver.exe'
export PATH=$PATH:$CHROMEDRIVER_PATH
source venv/bin/activate
python3 Main.py
