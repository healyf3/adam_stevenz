#!/usr/bin/env python3

#imports for debugging
import pdb
import logging

import re
import configparser
import threading
import time
from bs4 import BeautifulSoup
from selenium import webdriver
import selenium.common.exceptions
import sys
import os

from selenium.webdriver.chrome.options import Options  

logging.getLogger().setLevel(logging.INFO)
#pdb.set_trace() #setting first breakpoint
logging.disable(logging.CRITICAL)

cop_disableErrors = Options()  
cop_disableErrorsHeadless = Options()  
cop_disableErrors.add_argument('--log-level=3')
cop_disableErrorsHeadless.add_argument('--log-level=3')
cop_disableErrorsHeadless.add_argument('--headless')



__temp_file__ = None
__parsed_file__ = None
__driver__ = webdriver
__config__ = configparser.RawConfigParser()
__component_data_pairs__ = {}

#some of the components from the datafile aren't listed in here (glucose urine). Should I add them??
__uco_input_element_pairs__ =  {'alt': 'alaninine aminotransferase',
                                'albumin': 'albumin',
                                'alkaline phosphatase': 'alkaline phosphatase total',
                                'amylase': 'amylase serum',
                                'ast': 'aspartate aminotransferase',
                                'bilirubin indirect': 'bilirubin indirect',
                                'total bilirubin': 'bilirubin total',
                                'bilirubin direct': 'bilirubin, direct',
                                'blood urea nitrogen (bun)': 'blood urea nitrogen',
                                'calcium': 'calcium serum/plasma',
                                'bicarbonate': 'carbon dioxide',
                                'chloride': 'chloride serum/plasma',
                                'cholesterol': 'cholesterol',
                                'creatinine': 'creatinine serum/plasma',
                                'gamma glutamyl transferase (ggt)': 'gamma glutamyltransferase',
                                'glucose': 'glucose random serum/plasma',
                                'ldh': 'lactate dehydrogenase',
                                'lipase': 'lipase plasma',
                                'magnesium': 'magnesium serum',
                                'potassium': 'potassium serum/plasma',
                                'total protein': 'protein total serum/plasma',
                                'sodium': 'sodium serum/plasma',
                                'triglycerides': 'triglycerides',
                                'uric acid': 'uric acid serum'}
__is_test__ = False
#__is_test__ = True
__is_frozen__ = True
quit_script=False
headless = True
force_exe = False
#__join_threads__=False

#not yet set
__config_path__ = ''
__data_file_path__ = ''
#__chromedriver_path__ = '/mnt/c/ChromeDriver/windows_chromedriver.exe' 
__chromedriver_path__ = './windows_chromedriver.exe' 

def main():
    global __temp_file__
    global __config__
    global __driver__
    global __config_path__
    global __data_file_path__
    global __chromedriver_path__
    global __is_frozen__

    config_name = 'config.ini'

    # determine if application is a script file or frozen exe
    data_path = '' #changed from NONE to ''
    chrome_path = '' #changed from NONE to ''
    if getattr(sys, 'frozen', False):  #isn't this always going to return false because you are assigning a value to frozen???
        # If the application is run as a bundle, the pyInstaller bootloader
        # extends the sys module by a flag frozen=True and sets the app
        # path into variable _MEIPASS'.
        data_path = sys._MEIPASS + '\data'
        chrome_path = sys._MEIPASS + '\drivers'
        __is_frozen__ = True
        logging.warn("Is Frozen")
        logging.warn(data_path)
    else:
        application_path = os.path.dirname(os.path.abspath(__file__))
        __is_frozen__ = False
        logging.info("Is Not Frozen")
        logging.info(application_path)

    #TODO: for Adam, need to auto put chromdriver in his path. Right now it is stored in /usr/local/bin
    #TODO: expected location of chrome:
    #Mac path: /Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome
    #Windows XP: %HOMEPATH%\Local Settings\Application Data\Google\Chrome\Application\chrome.exe
    #Windows Vista: C:\Users%USERNAME%\AppData\Local\Google\Chrome\Application\chrome.exe
    #TODO: create logic to check os so this can run on mac and windows 
    __config_path__ = os.path.join(data_path, config_name)
    __data_file_path__ = os.path.join(data_path, 'datafile')
#    os.environ["PATH"] += os.pathsep + os.path.join(chrome_path,'chromedriver.exe')
#    __chromedriver_path__ = os.path.join(chrome_path, 'chromedriver.exe') #for windows
    #__chromedriver_path__ = os.path.join(chrome_path, 'chromedriver') #for mac

    logging.info("Printing config path")
    logging.info(__config_path__)
    logging.info("Printing data file path")
    logging.info(__data_file_path__)
    #assert os.path.exists(__config_path__)
    __config__.read(__config_path__)

    __temp_file__ = read_file(__data_file_path__) #what is this?? Need to find a way to detect whenever the datafile is changed

    start_driver() #starting selenium webdriver

    #comment out threads while debugging because debugger can't seem to handle them
    threading_run_program = threading.Thread(target=run_program)
    threading_run_program.start()

    threading_start_user = threading.Thread(target=start_user)
    threading_start_user.start()
    
    #testcase
    #check_file_for_update()

def start_driver():
    global __driver__
    try:
        if __is_frozen__:
            __driver__ = webdriver.Chrome(__chromedriver_path__)
        else:
            #TODO: make startup script put chromedriver in PATH
            if(headless):
                __driver__ = webdriver.Chrome(executable_path=__chromedriver_path__,chrome_options=cop_disableErrorsHeadless) 
                logging.info("headless")
            else:
                __driver__ = webdriver.Chrome(executable_path=__chromedriver_path__,chrome_options=cop_disableErrors)
                if __is_test__:
                    __driver__.get(r"C:\python_tests\html\chemistry_page.html")
                else:
                    __driver__.get(__config__.get('paths', 'login_page'))
                    fill_credentials()
    except FileNotFoundError:
        logging.error("Web driver not found")


def start_user():
    greet_user()
    get_user_input() #commented out for right now

def run_program():
    while not quit_script:
        check_file_for_update()

def greet_user():
    print('> Update the text file with new data to begin')

def get_user_input():
    while True:
        print("> Enter 'quit' or 'q' to exit the program and close the browser")
        print("> Enter 'exe' or 'e' to force fill survey")
        user_input = input()
        handle_user_input(user_input)

def read_file(file_path):
    file = open(file_path)
    file_text = file.readlines()
    return file_text

def handle_user_input(user_input):
    global force_exe
    global quit_script
    user_input = user_input.lower()
    if user_input == 'quit' or user_input == 'q':
        __driver__.quit()
        quit_script = 1
        sys.exit()

    elif user_input == 'exe' or user_input == 'e':
        force_exe = True

def check_file_for_update():
    global __config__
    global __temp_file__
    global headless
    global force_exe
    file_path = None

    if __is_frozen__:
        file_path = __data_file_path__
    else:
        __config__.read(__config_path__)
        #file_path = __config__.get(__data_file_path__) #logic doesn't really make sense to me
        file_path = __data_file_path__
    if (str(__temp_file__) != str(read_file(file_path))) or force_exe:
    #if (1): #testcase
        print('Changes to the file have been detected or forced execution')
        headless = False 
        force_exe = False
        start_driver()
        __temp_file__ = read_file(file_path)
        find_component_data_pairs_from_text_file(__data_file_path__)
        fill_survey()
    else:
        time.sleep(.5)

def fill_survey():
    from selenium.webdriver.support.ui import Select
    from selenium.webdriver.common.keys import Keys
    global __driver__
    global __config__
    global field_names
    valid_url = ''

    __config__.read(__config_path__)

    is_invalid_url = True
    while is_invalid_url:
        if __is_test__:
            #valid_url = 'file:///C:/Users/Nick/Desktop/FinalMICRF.html'
            valid_url = 'file:///C:/python_tests/html/chemistry_page.html'
        else:
            valid_url = __config__.get('paths', 'survey_url')
            __driver__.get(valid_url)
            #valid_url = valid_url[:len(valid_url)-6] ##what is this??
            #valid_url = 'file:///C:/python_tests/html/chemistry_page.html' #testcase
        
        logging.info('current url: ' + __driver__.current_url)
        logging.info('valid url: ' + valid_url)

        if __driver__.current_url == valid_url:
            is_invalid_url = False

            # select U of CO Hospital from drop down
            select = Select(__driver__.find_element_by_id('_ctl0_Content_R_header_LOC_DropDown'))
            if(not(__is_test__)):
                select.select_by_value('22') #Is this value always going to be 22??
                #pass #testcase

            # fill fields
            try:
                element_target_pairs = find_element_target_pairs_in_html(__driver__.page_source)
                logging.info(element_target_pairs) 
                for each in element_target_pairs:
                    input_field = __driver__.find_element_by_id(element_target_pairs[each])
                    input_field.clear()
                    input_field.send_keys(__component_data_pairs__[__uco_input_element_pairs__[each]])

            except selenium.common.exceptions.NoSuchElementException:
                print("> Cannot fill survey")

        else:
            print('> awaiting for survey URL')
            time.sleep(2)

def fill_credentials():
    username = __config__.get('credentials','username')
    password = __config__.get('credentials','password')
    input_field_usr = __driver__.find_element_by_id("session_username")
    input_field_usr.clear()
    input_field_usr.send_keys(username)
    input_field_pw = __driver__.find_element_by_id("session_password")
    input_field_pw.clear()
    input_field_pw.send_keys(password)
    login_press = __driver__.find_element_by_id("create_session_link")
    login_press.click()

def find_element_target_pairs_in_html(page_source):
    soup = BeautifulSoup(page_source, 'html.parser')
    soup_parts = soup.find_all('td', class_=False)
    element_ids = []
    for part in soup_parts:
        element_ids.append(part.text)

    element_ids = remove_false_element_IDs(element_ids)
    logging.info(element_ids)
    input_ids = find_input_field_IDs_in_HTML(page_source)
    element_target_pairs = {}

    for n in range(len(element_ids)):
        element_target_pairs[element_ids[n]] = input_ids[n]

    return element_target_pairs

def remove_false_element_IDs(IDs):
    # currently only works for U of CO data
    new_id_list = []
    for id in IDs:
        if id.lower() in __uco_input_element_pairs__:
            new_id_list.append(id.lower())
    return new_id_list


def find_input_field_IDs_in_HTML(page_source):
    return re.findall(r'_ctl0_Content_R_loclabcontainer_loclabcontainer_\d{6}__ctl0__Text', page_source)

def find_component_data_pairs_from_text_file(file):
    global __component_data_pairs__

    #parse data into list so we can read it easily
    f=open(file,"r")
    lines=f.readlines()
    component = []
    for x in range(len(lines)):
        lines[x] = lines[x].split('  ') #seperate double spaces from strings
        lines[x] = [y.strip() for y in lines[x]] #strip extra space and weird \n characters at last index of a line 
        lines[x] = list(filter(None,lines[x])) #filter out the index without any characters
        component.append([lines[x][0],lines[x][len(lines[x])-1]]) #grab component and its data. This grabbed the most recent datapoint.
    f.close()

    for x in range(len(component)):
        if component[x][0].lower() in __uco_input_element_pairs__.values():
            __component_data_pairs__[component[x][0].lower()] = component[x][1] #data of component


if __name__=="__main__":
    main()
