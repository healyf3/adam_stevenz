#! /bin/bash

bash -c "export CHROMEDRIVER_PATH=$ADAM_STEVENZ_PROJ'/windows_chromedriver.exe'"
bash -c "export PATH=$PATH:$CHROMEDRIVER_PATH"
bash -c "cd $ADAM_STEVENZ_PROJ"
bash -c "source venv/bin/activate"
bash -c "python3 Main.py"
